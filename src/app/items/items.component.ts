import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];

  constructor(public authService:AuthService, private db:AngularFireDatabase) { }

  ngOnInit() {//
    this.authService.user.subscribe(user => {//תביא לי את היוזר המחובר
      if (user != null){ 
      this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(//לך לדטה בייס ותביא לי רשימה ומנתבת לפי איך שאני רוצה
        items =>//מביאה לי את כל מה שצולם למשתנה אייטמס
        {
          this.items = [];//איפוס מערך שיהיה ריק
          items.forEach(//רצים על כל הצילום מסך
            item => {//כל משתנה יקרא אייטם
              let y = item.payload.toJSON();
              y["key"] = item.key;//כל uid הוא y
              this.items.push(y);
            }
          )
          console.log(this.items);
        }
        
      )
    }
    })
  }
}