import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() data:any;
  stock: boolean;
  name: string;
  price:string;
  key :string;

  showTheButton = false;
  deleteBut = false;

  
  cancel()
  {
   this.showTheButton = false; //שלא יציג את הכפתורים של מחיקה
   this.deleteBut = false;//שלא יציק את הכפתורים האחרים
  }

  checkChange()
  {
    this.itemsService.updateItem(this.key,this.stock);
  }

  showButton()//קורה כאשר העכבר נמצא על
  {
    this.showTheButton = true;//מציג את כפתור המחיקה
  }

  hideButton()//כאשר העבר לא נמצא על
  {
    this.showTheButton = false;//לא מציג את כפתור המחיקה
  }

  show2Buttons()//כאשר אני לוחצת על מחיקה
  {
    this.deleteBut = true;//יציג את 2 הכפתורים
    this.showTheButton = false;//כפתור המחיקה הראשון
  }

  deleteItem()//הכפתור שבאמת מוחק
  {
    console.log("key: " + this.key );
    this.itemsService.deleteItem(this.key);//קורה לאייטמסרוויס ושולח לפונקציה מחיקה את הקי
    this.showTheButton = false;
   this.deleteBut = false;
  }


  constructor(private itemsService:ItemsService) { }


  ngOnInit() {
    this.key = this.data.key;//מקבלים מהאבא את כל המידע לתוך דאטה, ולכן שומרים אותם בתוך משתנים שיצרנו
    this.name = this.data.name;
    this.price = this.data.price;
    this.stock = this.data.stock;
  }


}
