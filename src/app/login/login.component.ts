import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  code ='';
  message ='';
  hide = true;
  require = true;
  required = '';

  toLogin()
  {
    if(this.email == '' || this.password == '')//האם המיל ריק
    {
      this.required = "this input is required";
      this.require = false;
    }
    if (this.require)
    {
      this.authService.login(this.email,this.password)//שליחת פרטים לאוט סרוויס
      .then(value =>
      {
        this.router.navigate(['/items']);//העברה לעמוד הבא
      })
      .catch(err =>//במידה ויש הערות
      {
        this.code = err.code;
        this.message = err.message;
      })
    }
  }
  constructor(private router:Router, private authService:AuthService) { }

  ngOnInit() {
  }

}
